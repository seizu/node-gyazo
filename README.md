# Gyazo Server on Node.js

## Build

```
% npm i && npm run build
```

## Run Server

```
% npm start
```

## Configure

Export environments.

|name|default|
|---|---|
|GYAZO_VIEW_HOST[^1]|http://127.0.0.1:3000|
|GYAZO_VIEW_PATH|/view|
|GYAZO_UPLOAD_PATH|/upload|
|GYAZO_UPLOAD_FILE_PATH|uploads|
|GYAZO_LISTEN_PORT|3000|

[^1]: default port is 3000

Another way is put the `.env` file.  

```
GYAZO_VIEW_HOST="http://127.0.0.1:3000"
GYAZO_VIEW_PATH="/view"
GYAZO_UPLOAD_PATH="/upload"
GYAZO_UPLOAD_FILE_PATH="/uploads"
GYAZO_LISTEN_PORT="3000"
```

## Use in the Docker container

```
$ docker build -t node-gyazo . 
$ docker run -p 3000:3000 -v /opt/gyazo/uploads node-gyazo
```