import express from 'express';
import multer from 'multer';
import md5 from 'md5';
import fs from 'fs';
import path from 'path';
import dotenv from 'dotenv';

dotenv.config();

const VIEW_URL_HOST = process.env.GYAZO_VIEW_HOST || `http://127.0.0.1:3000`;
const VIEW_URL_PATH = process.env.GYAZO_VIEW_PATH || '/view';
const UPLOAD_URL_PATH = process.env.GYAZO_UPLOAD_PATH || '/upload';
const UPLOAD_FILE_PATH = process.env.GYAZO_UPLOAD_FILE_PATH || 'uploads';
const LISTEN_PORT = process.env.GYAZO_LISTEN_PORT || 3000;

const app = express();
const upload = multer({dest: UPLOAD_FILE_PATH});

app.use(VIEW_URL_PATH, express.static(UPLOAD_FILE_PATH));

app.post(UPLOAD_URL_PATH, upload.single('imagedata'), (req, res) => {
    const id = req.body.id;
    const buf = fs.readFileSync(req.file.path);
    const hash = md5(buf);

    if (!id) {
        res.setHeader('X-Gyazo-Id', md5(req.ip + Date.now()));
    }

    const newFileName = path.join(path.dirname(req.file.path), `${hash}.png`);
    fs.renameSync(req.file.path, newFileName);

    res.send(VIEW_URL_HOST + VIEW_URL_PATH + `/${hash}.png`);
});

app.listen(LISTEN_PORT);

console.log('Gyazo server running as ' + VIEW_URL_HOST);
